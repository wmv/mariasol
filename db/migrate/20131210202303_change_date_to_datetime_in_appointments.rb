class ChangeDateToDatetimeInAppointments < ActiveRecord::Migration
  def change
    change_column :appointments, :appointment_date, :datetime
  end
end
