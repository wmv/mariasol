class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :name
      t.string :gender
      t.string :id_num
      t.string :record_num
      t.string :address
      t.string :cell_no
      t.string :email
      t.datetime :dob
      t.string :employer
      t.string :plan
      t.text :notes

      t.timestamps
    end
  end
end
