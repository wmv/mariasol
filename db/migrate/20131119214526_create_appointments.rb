class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.string :details
      t.datetime :appointment_date
      t.string :special_supplies

      t.timestamps
    end
  end
end
