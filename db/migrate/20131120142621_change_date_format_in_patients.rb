class ChangeDateFormatInPatients < ActiveRecord::Migration
  def self.up
   change_column :patients, :dob, :date
  end

  def self.down
   change_column :patients, :dob, :datetime
  end
end
