class AddAvatarUrlToPatients < ActiveRecord::Migration
  def change
  	    add_column :patients, :avatar_url, :string
  end
end
