class AddCompletedToAppointments < ActiveRecord::Migration
  def change
    add_column :appointments, :completed, :integer
  end
end
