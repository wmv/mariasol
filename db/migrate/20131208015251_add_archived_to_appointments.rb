class AddArchivedToAppointments < ActiveRecord::Migration
  def change
    add_column :appointments, :archived, :integer
  end
end
