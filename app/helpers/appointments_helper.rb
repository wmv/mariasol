module AppointmentsHelper
	def appointments_chart_data
		(6.months.ago.to_date..Date.today).map do |date|
			{
				appointment_date: date,
				price: Appointment.where("date(appointment_date) = ?", date).sum(1)
			}
		end
	end
end
