# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery -> 
	ready = ->
		new Morris.Line(
	    element: "appointments_chart"
	    data: $('#appointments_chart').data('appointments')
	    xkey: 'appointment_date'
	    ykeys: ['price']
	    labels: ['Consultas']
	  )
	$(document).ready(ready)
	$(document).on('page:load', ready)