json.array!(@appointments) do |appointment|
  json.extract! appointment, :patient_id, :details, :appointment_date, :special_supplies
  json.url appointment_url(appointment, format: :json)
end
