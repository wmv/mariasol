json.array!(@patients) do |patient|
  json.extract! patient, :name, :gender, :id_num, :record_num, :address, :cell_no, :email, :dob, :employer, :plan, :notes
  json.url patient_url(patient, format: :json)
end
