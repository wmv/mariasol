class PatientsController < ApplicationController
  before_action :set_patient, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  # GET /patients
  # GET /patients.json

  def main_menu
    @patients = Patient.all
    @appointments = Appointment.all
    @on_index_page = true
  end
  
  def generate_pdf
    id_s = params[:pati]
    id = id_s
    p = Patient.find(id)
    if p.appointments.any?
      aps = p.appointments.find(:all)
      
      def text_date(ap) #takes an appointment object as input and returns its formatted date 
        ap.appointment_date.strftime("%d/%m/%Y as %H:%M")
      end
      
      ap_list = Array.new
      if aps.any?
        ap_list << ["<font size='20'><b>Data: </b></font>", "<font size='20'>Tipo</font>", "<font size='20'>Preco</font>"]
        aps.each do |a|
          ap_list << ["<font size='20'><b>#{text_date(a)}</b></font>", "<font size='20'>Consulta</font>", "<font size='20'> #{a.price.to_s} AKz </font>"]
        end
      end
      
      
      pdf = Prawn::Document.new
      pdf.text "\n\n\n", :inline_format => true
      pdf.text "<b><i>Clinica Familiar Sol Maria</i></b>", :size => 32, :align => :center, :inline_format => true
      pdf.text "<b>www.sol-maria.com</b>", :size => 15, :align => :center, :inline_format => true
      pdf.text "<b>Tel:</b> 923-12-34-56", :size => 15, :align => :center, :inline_format => true
      pdf.text "<b>Fax:</b> 222-45-54-23", :size => 15, :align => :center, :inline_format => true
      pdf.text "\n\n\n\n", :inline_format => true
      pdf.move_down 30    
      pdf.stroke_horizontal_rule
      pdf.move_down 10    
      pdf.text "<i>HISTORICO DE CONSULTAS</i>", :size => 13, :inline_format => true, :align => :center
      pdf.text "<b>Paciente: " + p.name + "</b>", :size => 13, :inline_format => true
      pdf.text "<b>Numero de Ficheiro: " + p.record_num + "</b>", :size => 13, :inline_format => true
      pdf.stroke_horizontal_rule
      pdf.move_down 30
      pdf.text "\n\n\n", :inline_format => true
      pdf.table(ap_list, 
                   :cell_style => { :inline_format => true }, :position => :center)
      filename = File.join(Rails.root, "historico.pdf")
      pdf.render_file filename
      send_file filename, :filename => p.name + ".pdf", :type => "application/pdf"
    else
      pdf = Prawn::Document.new
      pdf.text "\n\n\n", :inline_format => true
      pdf.text "<b><i>Clinica Familiar Sol Maria</i></b>", :size => 32, :align => :center, :inline_format => true
      pdf.text "<b>www.sol-maria.com</b>", :size => 15, :align => :center, :inline_format => true
      pdf.text "<b>Tel:</b> 923-12-34-56", :size => 15, :align => :center, :inline_format => true
      pdf.text "<b>Fax:</b> 222-45-54-23", :size => 15, :align => :center, :inline_format => true
      pdf.text "\n\n\n\n", :inline_format => true
      pdf.move_down 30    
      pdf.stroke_horizontal_rule
      pdf.move_down 10    
      pdf.text "<i>HISTORICO DE CONSULTAS</i>", :size => 13, :inline_format => true, :align => :center
      pdf.text "<b>Paciente: " + p.name + "</b>", :size => 13, :inline_format => true
      pdf.text "<b>Numero de Ficheiro: " + p.record_num + "</b>", :size => 13, :inline_format => true
      pdf.stroke_horizontal_rule
      pdf.move_down 30
      pdf.text "\n\n\n", :inline_format => true
      pdf.text "<b>Este paciente nao tem consultas no historico</b>", :size => 13, :inline_format => true
      filename = File.join(Rails.root, "historico.pdf")
      pdf.render_file filename
      send_file filename, :filename => p.name + ".pdf", :type => "application/pdf"
    end
  end

  #XXX add option to generate pdf with full patient history.
  def index
    @patients = Patient.all
    @appointments = Appointment.all
    @on_index_page = true
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
    @appointments = @patient.appointments.find(:all)
  end

  # GET /patients/new
  def new
    @patient = Patient.new
  end

  # GET /patients/1/edit
  def edit
  end

  def search
    @patients = Patient.search(params)
    @keyw = params[:keyword]
  end


  # POST /patients
  # POST /patients.json
  def create
    @patient = Patient.new(patient_params)
    
    respond_to do |format|
      if @patient.save
        format.html { redirect_to @patient, notice: 'Ficheiro do paciente ' + @patient.name + ' foi criado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @patient }
      else
        format.html { render action: 'new' }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
    
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    respond_to do |format|
      if @patient.update(patient_params)
        format.html { redirect_to @patient, notice: 'Ficheiro do paciente ' + @patient.name + ' foi editado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    patient_identification = @patient.id
    @patient.destroy
    respond_to do |format|
      Appointment.destroy_all(:patient_id => patient_identification)
      format.html { redirect_to patients_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient
      @patient = Patient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def patient_params
      params.require(:patient).permit(:name, :gender, :id_num, :record_num, :address, :cell_no, :email, :dob, :employer, :plan, :notes)
    end
end
