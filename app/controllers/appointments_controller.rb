class AppointmentsController < ApplicationController
  before_action :set_appointment, only: [:show, :edit, :update, :destroy]

  # GET /appointments
  # GET /appointments.json
  def generate_pdf
    id_s = params[:appo]
    id = id_s
    appointment = Appointment.find(id)
    patient = Patient.find(appointment.patient_id)
    text_date = appointment.appointment_date.strftime("%d/%m/%Y as %H:%M")
    pdf = Prawn::Document.new
    pdf.text "\n\n\n", :inline_format => true
    pdf.text "<b><i>Clinica Familiar Sol Maria</i></b>", :size => 32, :align => :center, :inline_format => true
    pdf.text "<b>www.sol-maria.com</b>", :size => 15, :align => :center, :inline_format => true
    pdf.text "<b>Tel:</b> 923-12-34-56", :size => 15, :align => :center, :inline_format => true
    pdf.text "<b>Fax:</b> 222-45-54-23", :size => 15, :align => :center, :inline_format => true
    pdf.text "<b>Endereco:</b>", :size => 15, :align => :center, :inline_format => true
    pdf.text "<b>Rua </b>XYZ", :size => 15, :align => :center, :inline_format => true
    pdf.text "<b>Bloco</b> HJK", :size => 15, :align => :center, :inline_format => true
    pdf.text "<b>Casa</b> 123", :size => 15, :align => :center, :inline_format => true
    pdf.text "<b>Horario de atendimento:</b>", :size => 15, :align => :center, :inline_format => true
    pdf.text "<b>Seg-Sex</b> - das 09:00 as 18:00", :size => 15, :align => :center, :inline_format => true
    pdf.text "<b>Sabado</b> - das 09:00 as 13:00", :size => 15, :align => :center, :inline_format => true
    pdf.text "\n\n\n\n", :inline_format => true
    pdf.move_down 30    
    pdf.stroke_horizontal_rule
    pdf.move_down 10    
    pdf.text "<i>RECIBO DE CONSULTA</i>", :size => 13, :inline_format => true, :align => :center
    pdf.stroke_horizontal_rule
    pdf.move_down 30
    pdf.text "\n\n\n", :inline_format => true
    pdf.table([  ["<font size='20'><b>Paciente: </b></font>", "<font size='20'><i>" + patient.name + "</i></font>"],
                 ["<font size='20'><b>Data Marcada: </b></font>", "<font size='20'><i>" + text_date + "</i></font>"],
                 ["<font size='20'><b>Preco da Consulta: </b></font>", "<font size='20'><i>" + appointment.price.to_s + " AKZ" + "</i></font>"] ], 
                 :cell_style => { :inline_format => true }, :position => :center)
    filename = File.join(Rails.root, "recibo.pdf")
    pdf.render_file filename
    send_file filename, :filename => patient.name + ".pdf", :type => "application/pdf"
  end
  
  def index
    @appointments = Appointment.get_incomplete.order("appointment_date desc")
    @patients = Patient.all
    @on_index_page = true
  end

  def history
    @appointments = Appointment.all.get_completed.order("appointment_date desc")
    @patients = Patient.all
  end

  # GET /appointments/1
  # GET /appointments/1.json

  #XXX Define tags that can be associated with consultations- - to allow to search by keyword.
  def show
    @patients = Patient.all
  end

  # GET /appointments/new
  def new
    @appointment = Appointment.new
    @patients = Patient.all
  end

  def mark_completed
    Appointment.update_all({completed: 1}, {id: params[:appointment_ids]})
    redirect_to appointments_url
  end

  # GET /appointments/1/edit
  def edit
    @patients = Patient.all
  end

  # POST /appointments
  # POST /appointments.json
  def create
    @appointment = Appointment.new(appointment_params)
    @patients = Patient.all
    
    respond_to do |format|
      if @appointment.save
         #nexmo = Nexmo::Client.new('62c6f18a', '1aeaa866')
         #sms_text = ("Consulta de " + @patients.find(@appointment.patient_id).name)
         #sms_text = sms_text + " foi marcada para " + appointment_params[:"appointment_date(3i)"]
         #sms_text = sms_text + " de " + appointment_params[:"appointment_date(2i)"]
         #sms_text = sms_text + " de " + appointment_params[:"appointment_date(1i)"]
          #nexmo.send_message!({:to => '27760714429', :from =>  'Ruby', :text => sms_text})
        format.html { redirect_to @appointment, notice: 'Consulta do paciente ' + @patients.find(@appointment.patient_id).name + ' foi criada com sucesso.' }
        format.json { render action: 'show', status: :created, location: @appointment }
      else
        format.html { render action: 'new' }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /appointments/1
  # PATCH/PUT /appointments/1.json
  def update
    @patients = Patient.all
    respond_to do |format|
      if @appointment.update(appointment_params)
        format.html { redirect_to @appointment, notice: 'Consulta do paciente ' + @patients.find(@appointment.patient_id).name + ' foi editada com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /appointments/1
  # DELETE /appointments/1.json
  def destroy
    @appointment.destroy
    respond_to do |format|
      format.html { redirect_to appointments_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_appointment
      @appointment = Appointment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def appointment_params
      params.require(:appointment).permit(:patient_id, :details, :appointment_date, :special_supplies, :completed, :price)
    end
end
