class Patient < ActiveRecord::Base
	validates :name, presence: true
	validates :id_num, presence: true
	validates :cell_no, presence: true
	validates :email, presence: true, uniqueness: true
	validates :record_num, presence: true
	has_many :appointments
	before_validation :prep_email
	before_save :create_avatar_url
	
	
	def self.search(params)
    searched_name = params[:keyword].strip
    searched_name = searched_name.downcase
    if searched_name.is_integer?
      if searched_name.length > 8
        where("cell_no LIKE ?", "%#{searched_name}%")
      elsif searched_name.length <= 7
        where("record_num LIKE ?", "%#{searched_name}%")
      end
    else
      if !searched_name.include? '@'
        where("name ILIKE ?", "%#{searched_name}%") 
      else
        where("email ILIKE ?", "%#{searched_name}%")
      end
    end
    
  end

	def prep_email
		self.email = self.email.strip.downcase if self.email
	end

	def create_avatar_url
		self.avatar_url = "http://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(self.email)}?s=200"
	end

end
