class Appointment < ActiveRecord::Base
  
  def self.get_archived
        where("archived = ?", 1)
  end
  
  def self.get_non_archived
        where("archived = ?", 0)
  end

  def self.get_completed
        where("completed = ?", 1)
  end

  def self.get_incomplete
        where("completed = ?", 0)
  end
	validates :patient_id, presence: true, length: { minimum: 1 }
	validates :appointment_date, presence: true
	validates :price, presence: true
	belongs_to :patient
end
